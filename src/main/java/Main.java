import java.util.Scanner;
import java.lang.Math;

public class Main {
    public static void main(String[] args) {
        program();
    }

    private static void program() {
        int choice;
        boolean isOn = true;
        String menu = "----------------------------------------\n" +
                "Kalkulator Penghitung Luas dan Volum\n" +
                "----------------------------------------\n" +
                "Menu\n" +
                "1. Hitung Luas Bidang\n" +
                "2. Hitung Volum\n" +
                "0. Tutup Aplikasi";
        String menuArea = "----------------------------------------\n" +
                "Pilih bidang yang akan dihitung\n" +
                "----------------------------------------\n" +
                "1. Persegi\n" +
                "2. Lingkaran\n" +
                "3. Segitiga\n" +
                "4. Persegi Panjang\n" +
                "0. Kembali ke menu sebelumnya";
        String menuVolume = "----------------------------------------\n" +
                "Pilih bidang yang akan dihitung\n" +
                "----------------------------------------\n" +
                "1. Kubus\n" +
                "2. Balok\n" +
                "3. Tabung\n" +
                "0. Kembali ke menu sebelumnya";

        while (isOn) {
            System.out.println(menu);
            choice = askInt("Masukkan pilihan anda : ");
            String operation;
            if (choice == 1) {
                System.out.println(menuArea);
                choice = askInt("Pilih bidang yang akan dihitung : ");
                operation = "Luas";
                if (choice == 1) {
                    System.out.println("----------------------------------------\n" +
                            "Anda memilih persegi\n" +
                            "----------------------------------------"
                    );
                    int side = askInt("Masukkan sisi persegi : ");
                    formattedAnswer(operation, "persegi", squareArea(side));
                } else if (choice == 2) {
                    System.out.println("----------------------------------------\n" +
                            "Anda memilih lingkaran\n" +
                            "----------------------------------------"
                    );
                    int radius = askInt("Masukkan jari-jari lingkaran : ");
                    formattedAnswer(operation, "lingkaran", circleArea(radius));
                } else if (choice == 3) {
                    System.out.println("----------------------------------------\n" +
                            "Anda memilih segitiga\n" +
                            "----------------------------------------"
                    );
                    int base = askInt("Masukkan alas segitiga : ");
                    int height = askInt("Masukkan tinggi segitiga : ");
                    formattedAnswer(operation, "segitiga", triangleArea(base, height));
                } else if (choice == 4) {
                    System.out.println("----------------------------------------\n" +
                            "Anda memilih persegi panjang\n" +
                            "----------------------------------------"
                    );
                    int width = askInt("Masukkan panjang persegi panjang : ");
                    int length = askInt("Masukkan lebar persegi panjang : ");
                    formattedAnswer(operation, "persegi panjang", rectangleArea(length, width));
                } else {
                    System.out.println("Masukan tidak valid");
                }
            } else if (choice == 2) {
                operation = "Volum";
                System.out.println(menuVolume);
                choice = askInt("Pilih bidang yang akan dihitung : ");
                if (choice == 1) {
                    System.out.println("----------------------------------------\n" +
                            "Anda memilih kubus\n" +
                            "----------------------------------------"
                    );
                    int edge = askInt("Masukkan panjang sisi kubus : ");
                    formattedAnswer(operation, "kubus", cubeVolume(edge));
                } else if (choice == 2) {
                    System.out.println("----------------------------------------\n" +
                            "Anda memilih balok\n" +
                            "----------------------------------------"
                    );
                    int width = askInt("Masukkan panjang balok : ");
                    int length = askInt("Masukkan lebar balok : ");
                    int height = askInt("Masukkan tinggi balok : ");
                    formattedAnswer(operation, "balok", cuboidVolume(width, length, height));
                } else if (choice == 3) {
                    System.out.println("----------------------------------------\n" +
                            "Anda memilih tabung\n" +
                            "----------------------------------------"
                    );
                    int radius = askInt("Masukkan jari-jari tabung : ");
                    int height = askInt("Masukkan tinggi tabung : ");
                    formattedAnswer(operation, "tabung", cylinderVolume(radius, height));
                } else {
                    System.out.println("Masukan tidak valid");
                }
            } else if (choice == 0) {
                isOn = false;
            } else {
                System.out.println("Masukkan tidak valid");
            }
        }
    }

    /**
     * Method to ask positif integer
     *
     * @param message asking message
     * @return positive integer
     */
    private static int askInt(String message) {
        Scanner input = new Scanner(System.in);
        int positiveNumber;
        do {
            System.out.print(message);
            positiveNumber = input.nextInt();
        }
        while (positiveNumber < 0);
        return positiveNumber;
    }

    /**
     * Method to format answer
     *
     * @param operation
     * @param type
     * @param answer
     */
    private static void formattedAnswer(String operation, String type, double answer) {
        Scanner input = new Scanner(System.in);
        System.out.println("Processing ...\n" +
                operation +
                " dari " +
                type +
                " adalah " +
                answer +
                "\n----------------------------------------\n" +
                "tekan apa saja untuk kembali ke menu utama"
        );
        input.nextLine();
    }

    /**
     * Method to calculate rectangle area
     *
     * @param length length of the rectangle
     * @param width  width of the rectangle
     * @return int rectangle area
     */
    private static int rectangleArea(int length, int width) {
        return length * width;
    }

    /**
     * Method to calculate square area
     *
     * @param side side of the square
     * @return int square area
     */
    private static int squareArea(int side) {
        return side * side;
    }


    /**
     * Method to calculate circle area
     *
     * @param radius radius of the circle
     * @return double circle area
     */
    private static double circleArea(int radius) {
        return Math.PI * Math.pow(radius, 2);
    }

    /**
     * Method to calculate triangle area
     *
     * @param base   base of the triangle
     * @param height height of the triangle
     * @return double triangle area
     */
    private static double triangleArea(double base, double height) {
        return (base * height) / 2;
    }

    /**
     * Method to calculate cube volume
     *
     * @param edge edge of the cube
     * @return int cube volume
     */
    private static int cubeVolume(int edge) {
        return (int) Math.pow(edge, 3);
    }

    /**
     * Method to calculate cuboid volume
     *
     * @param width  width of the cuboid
     * @param length length of the cuboid
     * @param height height of the cuboid
     * @return double of the cuboid volume
     */
    private static double cuboidVolume(double width, double length, double height) {
        return width * length * height;
    }

    /**
     * Method to calculate cylinder volume
     *
     * @param radius radius of the cylinder
     * @param height height of the cylinder
     * @return double cylinder volume
     */
    private static double cylinderVolume(double radius, double height) {
        return Math.PI * height * Math.pow(radius, 2);
    }
}
